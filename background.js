﻿function focusOrCreateTab(url) {
  chrome.windows.getAll({"populate":true}, function(windows) {
    var existing_tab = null;
    for (var i in windows) {
      var tabs = windows[i].tabs;
      for (var j in tabs) {
        var tab = tabs[j];
        if (tab.url == url) {
          existing_tab = tab;
          break;
        }
      }
    }
    if (existing_tab) {
      chrome.tabs.update(existing_tab.id, {"selected":true});
    } else {
      chrome.tabs.create({"url":url, "selected":true});
    }
  });
}
function getDomainFromUrl(url){
  var host = "null";
  if(typeof url == "undefined" || null == url)
    url = window.location.href;
  var regex = /.*\:\/\/([^\/]*).*/;
  var match = url.match(regex);
  if(typeof match != "undefined" && null != match)
    host = match[1];
  return host;
}
var adzoneid        =           '';
var siteid          = '';
chrome.storage.sync.get('adzoneid',function (item) {
  adzoneid        = item.adzoneid;
  console.log(item);
});
chrome.storage.sync.get('siteid',function (item) {
  siteid        = item.siteid;
});
var msg = {
  type: "setting",
  adzoneid : adzoneid,
  siteid:siteid

};

chrome.browserAction.onClicked.addListener(function(tab) {
  var manager_url = chrome.extension.getURL("setting.html");
  console.log(tab);
  focusOrCreateTab(manager_url);
});
chrome.tabs.onCreated.addListener(function(tab){
  console.log(tab);
  if(getDomainFromUrl(tab.url).toLowerCase()=="pub.alimama.com"){
    chrome.runtime.sendMessage(msg);
  }
});
console.log('back');
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo,tab) {
  console.log(tab);
  if(getDomainFromUrl(tab.url).toLowerCase()=="pub.alimama.com"){
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.tabs.sendMessage(tabs[0].id,msg, function(response) {
        console.log(response.farewell);
      });
    });
  }
});
chrome.tabs.onReload.addListener(function(tab){
  console.log(tab);
  if(getDomainFromUrl(tab.url).toLowerCase()=="pub.alimama.com"){
    chrome.runtime.sendMessage(msg);
  }
});
chrome.tabs.onActive.addListener(function(tab){
  console.log(tab);
  if(getDomainFromUrl(tab.url).toLowerCase()=="pub.alimama.com"){
    chrome.runtime.sendMessage(msg);
  }
});

